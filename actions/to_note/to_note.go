package to_note

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
	"gitlab.com/gomidi/midiline/value"
)

// TransformKey converts the given valuer to a note key with the given velocity.
// If the valuer return 0, a note off message is sent.
//
// Example:
//
//    // To get a transformer that converts cc1 messages on channel2 to note messages of a 8th note length and
//    // and velocity of 100, use
//    var m = midiline.And(typ.Channel2,message.ControlChange(1))
//    var t = TransformKey(100,4, m, value.ControlChange)
func ActionKey(velocity uint8, nt32th uint32, valuer midiline.Valuer) midiline.Action {
	return ActionKeyScale(velocity, nt32th, valuer, nil)
}

// TransformKeyScale is like TransformKey but allows a scaler function to calculate the note key
// value from the valuer value.
//
// Example:
//    // To get a transformer that converts cc1 messages on channel2 to note messages of a 8th note length by adding 1, use
//    var m = midiline.And(typ.Channel2,message.ControlChange(1))
//    var s = func (in uint8) (out uint8) {
//        return in+1 // lib takes care that the value won't get too high (>127)
//	  }
//
//    var t = TransformKeyScale(100,4,m,value.ControlChange,s)
func ActionKeyScale(velocity uint8, nt32th uint32, valuer midiline.Valuer, scale func(uint8) uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if !valuer.IsMet(in) {
			return in
		}

		val := valuer.Value(in)
		if scale != nil {
			val = scale(val)
		}

		var ch = channel.Channel0

		if chMsg, is := in.(channel.Message); is {
			ch = channel.Channel(chMsg.Channel())
		}

		if nt32th == 0 {
			nt32th = 1
		}

		// schedule the note off message
		ctx.Schedule((ctx.Resolution()*nt32th)/8, ch.NoteOff(val))
		return ch.NoteOn(val, velocity)
	})
}

// TransformVelocity converts the given valuer to a note velocity with the given key.
// If the valuer return 0, a note off message is sent.
//
// Example:
//
//    // To get a transformer that converts cc1 messages on channel2 to note velocities of key 64 and length 8th note, use
//    var m = midiline.And(typ.Channel2,message.ControlChange(1))
//    var t = TransformVelocity(64, 4,m, value.ControlChange)
func ActionVelocity(key uint8, nt32th uint32, valuer midiline.Valuer) midiline.Action {
	return ActionVelocityScale(key, nt32th, valuer, nil)
}

// TransformVelocityScale is like TransformVelocity but with a custom scaling function.
func ActionVelocityScale(key uint8, nt32th uint32, valuer midiline.Valuer, scale func(uint8) uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if !valuer.IsMet(in) {
			return in
		}

		val := valuer.Value(in)
		if scale != nil {
			val = scale(val)
		}

		var ch = channel.Channel0

		if chMsg, is := in.(channel.Message); is {
			ch = channel.Channel(chMsg.Channel())
		}

		if nt32th == 0 {
			nt32th = 1
		}

		// schedule the note off message
		ctx.Schedule((ctx.Resolution()*nt32th)/8, ch.NoteOff(key))
		return ch.NoteOn(key, val)
	})
}

type registryTransformerKey struct{}

func (t registryTransformerKey) Name() string {
	return "value.to.note.key"
}

func (t registryTransformerKey) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var valuer string
	var velocity uint8
	var nt32th uint32

	err := config.Parse(c, args, &velocity, &nt32th, &valuer)
	if err != nil {
		return nil, err
	}

	v := value.FindValuer(valuer)

	if v == nil {
		return nil, fmt.Errorf("%#v is not a value.condition", valuer)
	}

	return ActionKey(velocity, nt32th, v), nil
}

func init() {
	config.RegisterActionMaker(registryTransformerKey{}, "velocity [int], len32th [int], value.condition [string]")
}

type registryTransformerVelocity struct{}

func (t registryTransformerVelocity) Name() string {
	return "value.to.note.velocity"
}

func (t registryTransformerVelocity) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var valuer string
	var key uint8
	var nt32th uint32

	err := config.Parse(c, args, &key, &nt32th, &valuer)
	if err != nil {
		return nil, err
	}

	v := value.FindValuer(valuer)

	if v == nil {
		return nil, fmt.Errorf("%#v is not a value.condition", valuer)
	}

	return ActionVelocity(key, nt32th, v), nil
}

func init() {
	config.RegisterActionMaker(registryTransformerVelocity{}, "key [int], len32th [int], value.condition [string]")
}

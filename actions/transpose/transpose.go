package transpose

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

func transposeKey(original uint8, delta int8) uint8 {
	key := int16(original) + int16(delta)
	if key > 127 {
		key = 127
	}
	if key < 0 {
		key = 0
	}
	return uint8(key)
}

// Transform returns a new transposer that transposes by the given delta.
// It will keep the resulting key in the 0-127 range.
func Action(delta int8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		switch v := in.(type) {
		case channel.NoteOn:
			return channel.Channel(v.Channel()).NoteOn(transposeKey(v.Key(), delta), v.Velocity())
		case channel.NoteOff:
			return channel.Channel(v.Channel()).NoteOff(transposeKey(v.Key(), delta))
		case channel.NoteOffVelocity:
			return channel.Channel(v.Channel()).NoteOffVelocity(transposeKey(v.Key(), delta), v.Velocity())
		}
		return in
	})
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "note.transpose"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var delta int8
	err := config.Parse(c, args, &delta)
	if err != nil {
		return nil, err
	}
	return Action(delta), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "delta [int]")
}

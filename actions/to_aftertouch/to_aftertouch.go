package to_aftertouch

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
	"gitlab.com/gomidi/midiline/value"
)

// Action converts a message to a after touch message if the Condition of the valuer is met.
//
// Example:
//
//    // To get an action that converts MIDI notes to after touch messages, use
//    var act = Action(value.NoteKey)
func Action(valuer midiline.Valuer) midiline.Action {
	return ActionScale(valuer, nil)
}

// ActionScale is like Action but allows a scaler function to calculate the after touch
// value from the valuer value.
//
// Example:
//
//    // To get an action that converts MIDI note 64 to an aftertouch 4 message, use
//    var act = ActionScale(value.NoteKey, func (in uint8) (out uint8){ return in-60 })
func ActionScale(valuer midiline.Valuer, scaler func(uint8) uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if !valuer.IsMet(in) {
			return in
		}

		val := valuer.Value(in)

		if scaler != nil {
			val = scaler(val)
		}

		var ch = channel.Channel0

		if chMsg, is := in.(channel.Message); is {
			ch = channel.Channel(chMsg.Channel())
		}

		return ch.Aftertouch(val)
	})
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "value.to.aftertouch"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var valuer string

	err := config.Parse(c, args, &valuer)
	if err != nil {
		return nil, err
	}

	v := value.FindValuer(valuer)

	if v == nil {
		return nil, fmt.Errorf("%#v is not a value.condition", valuer)
	}

	return Action(v), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "value.condition [string]")
}

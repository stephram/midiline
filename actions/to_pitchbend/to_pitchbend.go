package to_pitchbend

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
	"gitlab.com/gomidi/midiline/value"
)

// Transform converts the given valuer to a pitch bend message if the given matcher matches.
//
// Example:
//
//    // To get a transformer that converts MIDI notes on channel2 to pitch bend messages of the double value of the key, use
//    var m = midiline.And(typ.Channel2,message.NoteKey(64,false))
//    var t = Transform(m, value.NoteKey, func (in uint8) (out int16){ return int16(in)*2 })
func Action(valuer midiline.Valuer, scaler func(uint8) int16) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if !valuer.IsMet(in) {
			return in
		}

		val := valuer.Value(in)
		var pVal = int16(val)

		if scaler != nil {
			pVal = scaler(val)
		}

		var ch = channel.Channel0

		if chMsg, is := in.(channel.Message); is {
			ch = channel.Channel(chMsg.Channel())
		}

		return ch.Pitchbend(pVal)
	})
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "value.to.pitchbend"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var valuer string
	err := config.Parse(c, args, &valuer)
	if err != nil {
		return nil, err
	}

	v := value.FindValuer(valuer)

	if v == nil {
		return nil, fmt.Errorf("%#v is not a value.condition", valuer)
	}

	converter := func(in uint8) int16 {
		if in > 127 {
			in = 127
		}
		res := 64 - in
		return int16(res)
	}

	return Action(v, converter), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "value.condition [string]")
}

package midiline

import "gitlab.com/gomidi/midi"

type Valuer interface {
	Condition
	Value(midi.Message) uint8
}

/*
type ValueFunc func(midi.Message) uint8

func (m ValueFunc) Value(msg midi.Message) uint8 {
	return m(msg)
}
*/

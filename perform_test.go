package midiline

import (
	"fmt"
	"runtime"
	"strconv"
	"testing"
	"time"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midi/testdrv"
)

func testPerformance(name string, line *Line, times int, maxDur time.Duration, message []byte) (time.Duration, error) {
	drv := testdrv.New(name)
	in, err := midi.OpenIn(drv, 0, "")
	if err != nil {
		return 0, err
	}
	out, err := midi.OpenOut(drv, 0, "")

	if err != nil {
		return 0, err
	}

	mDur := maxDur.Nanoseconds()

	var last time.Time
	//	var diff time.Duration

	in.SetListener(func(b []byte, _ int64) {
		diff := time.Now().Sub(last).Nanoseconds()
		d, _ := strconv.Atoi(string(b))
		dd := int64(d)
		if diff-dd > mDur {
			panic(fmt.Sprintf("%d > %d", diff-dd, mDur))
		}

		//		fmt.Printf("got % X\n", b)

		runtime.Gosched()
	})

	runner := ConnectSlim(in, out, mid.NoLogger())
	go func() {
		stop, err := runner.Run(line)
		if err != nil {
			return
		}
		time.Sleep(time.Second * 3)
		stop <- true
	}()

	last = time.Now()

	for i := 0; i < times; i++ {
		diff := time.Now().Sub(last).Nanoseconds()
		//		fmt.Printf("sending\n")
		//n := last.UnixNano()
		out.Send([]byte(fmt.Sprintf("%d", diff)))
		runtime.Gosched()
	}
	time.Sleep(time.Second)

	return 0, nil
}

func TestPerformEmptyLine(t *testing.T) {

	tests := []struct {
		times  int
		maxDur time.Duration
	}{
		{1000, time.Microsecond * 50},
		{10000, time.Microsecond * 50},
		{100000, time.Microsecond * 50},
		//		{1000000, time.Microsecond * 50},
	}

	for _, test := range tests {
		line := New("line.empty")
		msg := []byte{0x92, 0xF5, 0x34}
		dur, err := testPerformance("testperform1", line, test.times, test.maxDur, msg)

		if err != nil {
			t.Fatal(err)
		}

		if dur > test.maxDur {
			t.Errorf("time to send and receive %v messages is > %v: %v", test.times, test.maxDur, dur)
		}
	}
	//	fmt.Printf("time: %v", dur)
}

package message

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

type registryNoteKey struct{}

func (t registryNoteKey) Name() string {
	return "msg.note.key"
}

func (t registryNoteKey) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var key uint8
	var ignoreNoteOff bool
	err := config.Parse(c, args, &key, &ignoreNoteOff)
	if err != nil {
		return nil, err
	}

	return NoteKey(key, ignoreNoteOff), nil
}

func init() {
	config.RegisterConditionMaker(registryNoteKey{}, "key [int], ignoreNoteoff [bool]")
}

// NoteKey returns a Condition that matches for
func NoteKey(key uint8, ignoreNoteOff bool) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.NoteOn:
			return m.Key() == key
		case channel.NoteOff:
			if ignoreNoteOff {
				return false
			}
			return m.Key() == key
		case channel.NoteOffVelocity:
			if ignoreNoteOff {
				return false
			}
			return m.Key() == key
		default:
			return false
		}
	})
}

type registryNoteKeyRange struct{}

func (t registryNoteKeyRange) Name() string {
	return "msg.note.key.range"
}

func (t registryNoteKeyRange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var min, max uint8
	var ignoreNoteOff bool
	err := config.Parse(c, args, &min, &max, &ignoreNoteOff)
	if err != nil {
		return nil, err
	}

	return NoteKeyRange(min, max, ignoreNoteOff), nil
}

func init() {
	config.RegisterConditionMaker(registryNoteKeyRange{}, "minkey [int], maxkey [int], ignoreNoteoff [bool]")
}

func NoteKeyRange(min, max uint8, ignoreNoteOff bool) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.NoteOn:
			k := m.Key()
			return k <= max && k >= min
		case channel.NoteOff:
			if ignoreNoteOff {
				return false
			}
			k := m.Key()
			return k <= max && k >= min

		case channel.NoteOffVelocity:
			if ignoreNoteOff {
				return false
			}
			k := m.Key()
			return k <= max && k >= min
		default:
			return false
		}
	})
}

type registryNoteVelocityRange struct{}

func (t registryNoteVelocityRange) Name() string {
	return "msg.note.velocity.range"
}

func (t registryNoteVelocityRange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var min, max uint8
	err := config.Parse(c, args, &min, &max)
	if err != nil {
		return nil, err
	}

	return NoteVelocityRange(min, max), nil
}

func init() {
	config.RegisterConditionMaker(registryNoteVelocityRange{}, "minvel [int], maxvel [int]")
}

func NoteVelocityRange(min, max uint8) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.NoteOn:
			k := m.Velocity()
			return k <= max && k >= min
		default:
			return false
		}
	})
}

type registryControlChange struct{}

func (t registryControlChange) Name() string {
	return "msg.controlchange"
}

func (t registryControlChange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var controller uint8
	err := config.Parse(c, args, &controller)
	if err != nil {
		return nil, err
	}

	return ControlChange(controller), nil
}

func init() {
	config.RegisterConditionMaker(registryControlChange{}, "controller [int]")
}

func ControlChange(controller uint8) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.ControlChange:
			return controller == m.Controller()
		default:
			return false
		}
	})
}

type registryControlChangeRange struct{}

func (t registryControlChangeRange) Name() string {
	return "msg.controlchange.range"
}

func (t registryControlChangeRange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var min, max uint8
	err := config.Parse(c, args, &min, &max)
	if err != nil {
		return nil, err
	}

	return ControlChangeRange(min, max), nil
}

func init() {
	config.RegisterConditionMaker(registryControlChangeRange{}, "fromController [int], toController [int]")
}

func ControlChangeRange(min, max uint8) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.ControlChange:
			k := m.Controller()
			return k <= min && k >= max
		default:
			return false
		}
	})
}

type registryPolyphonicAftertouchKey struct{}

func (t registryPolyphonicAftertouchKey) Name() string {
	return "msg.polyaftertouch.key"
}

func (t registryPolyphonicAftertouchKey) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var key uint8
	err := config.Parse(c, args, &key)
	if err != nil {
		return nil, err
	}

	return PolyAftertouchKey(key), nil
}

func init() {
	config.RegisterConditionMaker(registryPolyphonicAftertouchKey{}, "key [int]")
}

func PolyAftertouchKey(key uint8) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.PolyAftertouch:
			return m.Key() == key
		default:
			return false
		}
	})
}

type registryPitchBendRange struct{}

func (t registryPitchBendRange) Name() string {
	return "msg.pitchbend.range"
}

func (t registryPitchBendRange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var min, max int16
	err := config.Parse(c, args, &min, &max)
	if err != nil {
		return nil, err
	}

	return PitchbendRange(min, max), nil
}

func init() {
	config.RegisterConditionMaker(registryPitchBendRange{}, "minVal [int], maxVal [int]")
}

func PitchbendRange(min, max int16) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		switch m := msg.(type) {
		case channel.Pitchbend:
			val := m.Value()
			return val <= min && val >= max
		default:
			return false
		}
	})
}

package typ

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/midimessage/realtime"
	"gitlab.com/gomidi/midi/midimessage/syscommon"
	"gitlab.com/gomidi/midi/midimessage/sysex"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

func init() {
	config.RegisterConditionMaker(All, "")
	config.RegisterConditionMaker(SysExMsg, "")
	config.RegisterConditionMaker(SysCommonMsg, "")
	config.RegisterConditionMaker(RealtimeMsg, "")
	config.RegisterConditionMaker(NoteMsg, "")
	config.RegisterConditionMaker(PitchbendMsg, "")
	config.RegisterConditionMaker(AftertouchMsg, "")
	config.RegisterConditionMaker(PolyAftertouchMsg, "")
	config.RegisterConditionMaker(ProgramChangeMsg, "")
	config.RegisterConditionMaker(ControlChangeMsg, "")
	config.RegisterConditionMaker(ChannelMsg, "")
	config.RegisterConditionMaker(ChannelAny, "")
	config.RegisterConditionMaker(Channel0, "")
	config.RegisterConditionMaker(Channel1, "")
	config.RegisterConditionMaker(Channel2, "")
	config.RegisterConditionMaker(Channel3, "")
	config.RegisterConditionMaker(Channel4, "")
	config.RegisterConditionMaker(Channel5, "")
	config.RegisterConditionMaker(Channel6, "")
	config.RegisterConditionMaker(Channel7, "")
	config.RegisterConditionMaker(Channel8, "")
	config.RegisterConditionMaker(Channel9, "")
	config.RegisterConditionMaker(Channel10, "")
	config.RegisterConditionMaker(Channel11, "")
	config.RegisterConditionMaker(Channel12, "")
	config.RegisterConditionMaker(Channel13, "")
	config.RegisterConditionMaker(Channel14, "")
	config.RegisterConditionMaker(Channel15, "")
}

type registryLineTick struct{}

func (t registryLineTick) Name() string {
	return "typ.linetick"
}

func (t registryLineTick) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return LineTick(), nil
}

func init() {
	config.RegisterConditionMaker(registryLineTick{}, "")
}

// LineTick returns a matcher that matches if msg is a LineTick.
func LineTick() midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		return msg == midiline.LineTick
	})
}

var names = map[Type]string{
	All:               "all",
	SysExMsg:          "sysex",
	MetaMsg:           "meta",
	SysCommonMsg:      "syscommon",
	RealtimeMsg:       "realtime",
	NoteMsg:           "note",
	PitchbendMsg:      "pitchbend",
	AftertouchMsg:     "aftertouch",
	PolyAftertouchMsg: "polyaftertouch",
	ProgramChangeMsg:  "programchange",
	ControlChangeMsg:  "controlchange",
	ChannelMsg:        "channel",
	ChannelAny:        "channelany",
	Channel0:          "channel0",
	Channel1:          "channel1",
	Channel2:          "channel2",
	Channel3:          "channel3",
	Channel4:          "channel4",
	Channel5:          "channel5",
	Channel6:          "channel6",
	Channel7:          "channel7",
	Channel8:          "channel8",
	Channel9:          "channel9",
	Channel10:         "channel10",
	Channel11:         "channel11",
	Channel12:         "channel12",
	Channel13:         "channel13",
	Channel14:         "channel14",
	Channel15:         "channel15",
}

func (t Type) Name() string {
	return "typ." + names[t]
}

func (t Type) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	if t > ChannelAny {
		return Join(ChannelMsg, t), nil
	}

	return t, nil
}

// Type is a flag that indicates interest in certain types of midi messages
type Type int

// Join joins several filters and returnes the joined filter
func Join(f ...Type) (combined Type) {
	for _, flag := range f {
		combined = combined | flag
	}
	return
}

// Includes returns whether o is included in f
func (f Type) Includes(o Type) bool {
	return f&o != 0
}

// MatchesChannelMsg returns whether the given channel message matches the filter
// It does not check, if the channel itself matches. For such a complete match see Filter.Match
func (t Type) isMetChannelMsg(msg channel.Message) bool {
	if t.Includes(ChannelMsg) {
		return true
	}
	switch msg.(type) {
	case channel.NoteOn, channel.NoteOff, channel.NoteOffVelocity:
		return t.Includes(NoteMsg)
	case channel.Aftertouch:
		return t.Includes(AftertouchMsg)
	case channel.PolyAftertouch:
		return t.Includes(PolyAftertouchMsg)
	case channel.Pitchbend:
		return t.Includes(PitchbendMsg)
	case channel.ControlChange:
		return t.Includes(ControlChangeMsg)
	case channel.ProgramChange:
		return t.Includes(ProgramChangeMsg)
	}

	panic("unreachable")
}

// Matches return wether the given midi.Message matches the Filter.
func (t Type) IsMet(msg midi.Message) bool {
	if msg == midiline.LineTick || msg == nil {
		return true
	}

	if t.Includes(All) {
		return true
	}

	switch msg.(type) {
	case meta.Message:
		return t.Includes(MetaMsg)
	case realtime.Message:
		return t.Includes(RealtimeMsg)
	case sysex.Message:
		return t.Includes(SysExMsg)
	case syscommon.Message:
		return t.Includes(SysCommonMsg)
	}

	/* must be a channel message */
	cm := msg.(channel.Message)
	if t.Includes(ChannelMsg) && t.Includes(ChannelAny) {
		return true
	}
	if t.Includes(ChannelAny) {
		return t.isMetChannelMsg(cm)
	}
	return t.Includes(Channel(cm.Channel())) && t.isMetChannelMsg(cm)
	panic("unreachable")
}

const (
	_ = iota

	// All will include every midi message
	All Type = 1 << iota

	// SysExclusiveMsg will include system exclusive messages
	SysExMsg

	// MetaMsg will include meta messages
	MetaMsg

	// SysCommonMsg will include system common messages
	SysCommonMsg

	// RealtimeMsg will include realtime messages
	RealtimeMsg

	// NoteMsg will include note on and note off messages. Must be used in conjunction with a channel filter
	NoteMsg

	// PitchbendMsg will include pitch bend messages. Must be used in conjunction with a channel filter
	PitchbendMsg

	// AftertouchMsg will include after touch messages. Must be used in conjunction with a channel filter
	AftertouchMsg

	// PolyAftertouchMsg will include polyphinc after touch messages. Must be used in conjunction with a channel filter
	PolyAftertouchMsg

	// ProgramChangeMsg will include program change messages. Must be used in conjunction with a channel filter
	ProgramChangeMsg

	// ControlChangeMsg will include control messages. Must be used in conjunction with a channel filter
	ControlChangeMsg

	// ChannelAny will any midi channel pass through
	ChannelAny

	// Channel0 will let messages of channel 0 pass through etc.
	Channel0
	Channel1
	Channel2
	Channel3
	Channel4
	Channel5
	Channel6
	Channel7
	Channel8
	Channel9
	Channel10
	Channel11
	Channel12
	Channel13
	Channel14
	Channel15
)

var _ midiline.Condition = Channel0

const (
	// ChannelMsg will include all channel messages. However it must be used in conjunction with a
	// channel message filter, you are interested in, e.g. Channel1 and AllChannelMsg will include
	// all channel messages with midi channel 2 (channels are starting from 0 here)
	ChannelMsg = NoteMsg |
		PitchbendMsg |
		AftertouchMsg |
		PolyAftertouchMsg |
		ProgramChangeMsg |
		ControlChangeMsg
)

// Channel returns the Type for the given channel number.
// It panics for channel numbers outside the valid range 0-15
func Channel(no uint8) Type {
	switch no {
	case 0:
		return Channel0
	case 1:
		return Channel1
	case 2:
		return Channel2
	case 3:
		return Channel3
	case 4:
		return Channel4
	case 5:
		return Channel5
	case 6:
		return Channel6
	case 7:
		return Channel7
	case 8:
		return Channel8
	case 9:
		return Channel9
	case 10:
		return Channel10
	case 11:
		return Channel11
	case 12:
		return Channel12
	case 13:
		return Channel13
	case 14:
		return Channel14
	case 15:
		return Channel15
	default:
		panic("invalid channel number")
	}
}

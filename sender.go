package midiline

import (
	"fmt"
	"io"
	"runtime"
	"time"

	"gitlab.com/gomidi/midi"
)

type sender struct {
	out               midi.Writer
	projectedMessages scheduleMessages
	down              bool
	log               bool
	stdout            io.Writer
}

func (s *sender) send(msg midi.Message) {
	go s.out.Write(msg)
}

func (s *sender) run(closer chan bool, msgch chan midi.Message, projectedch chan scheduledMessage) {
	for {
		select {
		case <-closer:
			fmt.Fprintln(s.stdout, "closing...")
			//			fmt.Println("closing...")
			close(msgch)
			close(closer)
			close(projectedch)
			s.down = true
			return
		case m := <-msgch:
			//fmt.Printf("got message: %v\n", m)
			// no need to consolidate notes any longer, since the mid.Writer does it for us
			if s.log {
				fmt.Fprintf(s.stdout, "=> %v\n", m)
				//				fmt.Printf("=> %v\n", m)
			}
			s.send(m)
			runtime.Gosched()

		// other messages
		case proj := <-projectedch:
			runtime.Gosched()
			//fmt.Printf("got projected message: %v for %v\n", proj.msg, proj.timestamp)
			go func() {
				time.Sleep(proj.timestamp.Sub(time.Now()))
				//fmt.Printf("now sending projected message: %v\n", proj.msg)
				msgch <- proj.msg
			}()

		default:
			runtime.Gosched()
		}
	}
}

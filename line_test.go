package midiline

import (
	"testing"
)

func TestLineCheck(t *testing.T) {

	var (
		a  = New("a")
		b  = New("b", a.Embed())
		c  = New("c", b.Embed())
		d  = New("d")
		e  = New("e")
		f  = New("f", e.Embed())
		ff = New("f", f.Embed())
	)
	a.actions = append(a.actions, c.Embed())
	d.actions = append(d.actions, d.Embed())

	var tests = []struct {
		input       *Line
		expected    error
		description string
	}{

		{New("test"), nil, "single line"},
		/*
			{New("test", New("test").Embed()), DuplicatedLineError("test"), "line including line of same name (1 level)"},
			{New("test", New("other", New("test").Embed()).Embed()), DuplicatedLineError("test"), "line including line of same name (2 level)"},
		*/
		//{New("test", New("other").Embed(), New("other").Embed()), nil, "line including line of same name in different threads"},
		// LineEmbeddingLoopError
		{a, LineEmbeddingLoopError("a -> c -> b -> a"), "indirect loop 1"},
		{b, LineEmbeddingLoopError("b -> a -> c -> b"), "indirect loop 2"},
		{d, LineEmbeddingLoopError("d -> d"), "direct loop"},
		{New("test", New("test2").Embed()), nil, "embed without loop 1"},
		{New("test", f.Embed(), e.Embed()), nil, "embed without loop 2"},
		{New("test", f.Embed(), ff.Embed()), nil, "embed without loop 3"},
	}

	for _, test := range tests {
		if got, expected := test.input.Check(), test.expected; got != expected {
			t.Errorf("test %#v failed: New(%#v).Check() == %v; expected %v", test.description, test.input.Name(), got, expected)
		}
	}
}

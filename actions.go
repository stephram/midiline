package midiline

import "gitlab.com/gomidi/midi"

// Pass returns an action that passes the message through the given action
// if the Condition is met and returns the original message otherwise.
func Pass(cond Condition, act Action) Action {
	return ActionFunc(func(ctx Context, msg midi.Message) midi.Message {
		if cond.IsMet(msg) {
			return act.Perform(ctx, msg)
		}
		return msg
	})
}

// Block returns an action that passes the message through the given action
// if the Condition is not met and returns the original message otherwise.
func Block(cond Condition, act Action) Action {
	return ActionFunc(func(ctx Context, msg midi.Message) midi.Message {
		if !cond.IsMet(msg) {
			return act.Perform(ctx, msg)
		}
		return msg
	})
}

// Condition for a midi.Message
type Condition interface {
	// IsMet returns whether the condition is met by the given midi.Message
	IsMet(msg midi.Message) bool
}

type ConditionFunc func(msg midi.Message) bool

func (m ConditionFunc) IsMet(msg midi.Message) bool {
	return m(msg)
}

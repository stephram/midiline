package midiline

import (
	"fmt"
	"io"
	"os"
	"sort"
	"time"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/smf"
	"gitlab.com/gomidi/midi/smf/smfwriter"
)

// TODO we have a problem, sir!
// tempo and timeSignatures may have been written in any track
// in a multitrack scenario, so we would need to go through all the tracks
// just to find them an then again to iterate through the stack

// smfCtx is used to read an SMF file, pipe it through the stack of transformers
// and write the transformed data to another SMF file
// Currently only SMF0 files are supported.
type smfCtx struct {
	tick                        uint64
	track                       uint32 // counting from 0
	bar                         uint32 // counting from 0
	beat                        uint8  // beat in a bar counting from 0 (in 16th)
	absTicksoflastTimeSigChange uint64
	absTickofLastBeatCalc       uint64
	tempo                       float64
	timeSignature               [2]uint8
	st                          *Line
	resolution                  uint32
	rd                          *mid.Reader
	wr                          writer // *smfWriter
	sleepAfterTick              bool
	inFile                      string
	outFile                     string
	numTracks                   uint16
	scheduledMessages           scheduledTickMessages
	opts                        []mid.ReaderOption
	stop                        chan bool
	stopable                    *stopableReader2
	stdout                      io.Writer
	stderr                      io.Writer
}

type writer interface {
	writeMessage(ctx Context, msg midi.Message)
}

type stopableReader2 struct {
	rd      io.Reader
	outfile *os.File
	stopped bool
	closed  bool
}

func (s *stopableReader2) Close() error {
	if s.closed || s.rd == nil {
		return io.EOF
	}
	if cl, is := s.rd.(io.ReadCloser); is {
		cl.Close()
	}
	s.outfile.Close()
	s.closed = true
	return nil
}

func (s *stopableReader2) Read(b []byte) (int, error) {
	if s.closed {
		return 0, io.EOF
	}
	if s.stopped {
		s.Close()
		return 0, io.EOF
	}
	if s.rd == nil {
		return 0, io.EOF
	}
	n, err := s.rd.Read(b)
	if err == io.EOF {
		s.Close()
	}
	return n, err
}

var _ io.ReadCloser = &stopableReader2{}

func (c *smfCtx) Schedule(deltaTicks uint32, msg midi.Message) {
	c.scheduledMessages = append(c.scheduledMessages, scheduledTickMessage{msg, c.tick + uint64(deltaTicks)})
	sort.Sort(c.scheduledMessages)
}

func (c *smfCtx) Bar() uint32 {
	return c.bar
}

func (c *smfCtx) Beat() uint8 {
	return c.beat
}

func (c *smfCtx) Resolution() uint32 {
	return c.resolution
}

func (c *smfCtx) TempoBPM() float64 {
	return c.tempo
}

func (c *smfCtx) Tick() uint64 {
	return c.tick
}

func (c *smfCtx) TimeSig() (num, denom uint8) {
	return c.timeSignature[0], c.timeSignature[1]
}

func (c *smfCtx) Track() uint32 {
	return c.track
}

func (c *smfCtx) waitForStop() {
	for {
		select {
		case <-c.stop:
			fmt.Fprintln(c.stdout, "closing SMF context")
			//			fmt.Println("closing SMF context")
			c.wr.writeMessage(c, meta.EndOfTrack)
			c.stopable.Close()
			c.sleepAfterTick = false
			close(c.stop)
			return
		}
	}
}

func newSMFCtx(inFile, outFile string, opts ...mid.ReaderOption) *smfCtx {
	ctx := &smfCtx{}
	ctx.inFile = inFile
	ctx.outFile = outFile
	ctx.opts = opts
	ctx.stdout = os.Stdout
	ctx.stderr = os.Stderr
	return ctx
}

func (ctx *smfCtx) runHeader() (header smf.Header, err error) {
	ctx.rd = mid.NewReader(ctx.opts...)
	ctx.rd.SMFHeader = func(h smf.Header) {
		fmt.Println(h)
	}
	header, err = ctx.rd.ReadSMFFileHeader(ctx.inFile)
	if err != nil {
		return
	}

	if header.Format != smf.SMF0 {
		err = fmt.Errorf("only supporting SMF0 (one track files) currently")
		return
	}

	ctx.numTracks = header.NumTracks

	if met, isMetric := header.TimeFormat.(smf.MetricTicks); isMetric {
		ctx.resolution = uint32(met.Resolution())
	} else {
		err = fmt.Errorf("unsupported time base in SMF: %v (only MetricTicks supported at the moment)", header.TimeFormat)
	}
	return
}

// Start starts the reading and writing
func (ctx *smfCtx) Run(st *Line) (stop chan<- bool, err error) {
	err = st.Check()
	if err != nil {
		return nil, fmt.Errorf("can't run smf context, because an error happened during the line check: %v", err)
	}
	ctx.stop = make(chan bool)
	ctx.tempo = 120
	ctx.timeSignature = [2]uint8{4, 4}
	ctx.st = st
	header, err := ctx.runHeader()
	if err != nil {
		return ctx.stop, err
	}

	file, err := os.Create(ctx.outFile)

	if err != nil {
		return ctx.stop, err
	}

	var opts []smfwriter.Option

	opts = append(opts, smfwriter.TimeFormat(header.TimeFormat))
	opts = append(opts, smfwriter.Format(header.Format))

	ctx.wr = newSmfWriter(mid.NewSMF(file, header.NumTracks, opts...))
	ctx.rd = mid.NewReader(ctx.opts...)
	ctx.rd.Msg.Each = ctx.gotMessage
	f, err := os.Open(ctx.inFile)
	if err != nil {
		return ctx.stop, err
	}
	ctx.stopable = &stopableReader2{rd: f, outfile: file}
	go ctx.waitForStop()
	go ctx.rd.ReadAllSMF(ctx.stopable)
	return ctx.stop, err
}

// popScheduledMessages finds the scheduled messages for the current tick
// and removes them from the scheduleMessages slice
func (ctx *smfCtx) popScheduledMessages() (scheduled []midi.Message) {
	var rest scheduledTickMessages
	for _, sm := range ctx.scheduledMessages {
		if sm.abstick > ctx.tick {
			rest = append(rest, sm)
		} else {
			scheduled = append(scheduled, sm.msg)
		}
	}
	sort.Sort(rest) // should not be neccessary, but who knows...
	ctx.scheduledMessages = rest
	return
}

// runThroughStack calculates bars and beats before running through the stack
func (ctx *smfCtx) runThroughStack(msg midi.Message) {
	// don't do the same tick twice!
	if ctx.tick > ctx.absTickofLastBeatCalc {

		// calc the beat and the bar and then run through the stack

		ticksWithinBar := calcTicksWithinBar(
			ctx.tick, ctx.absTicksoflastTimeSigChange,
			uint64(ctx.resolution),
			uint64(ctx.timeSignature[0]), uint64(ctx.timeSignature[1]))

		if ticksWithinBar == 0 {
			// a bar change just happened, but only
			ctx.bar++
			ctx.beat = 0
		} else {
			ctx.beat = calculateBeat(ticksWithinBar, ctx.resolution)
		}

		ctx.absTickofLastBeatCalc = ctx.tick
	}

	// now run through the stack
	msgs := ctx.st._runThroughLine(ctx, msg)

	// consolidate the note messages
	// since we can schedule messages and write new messages on every tick via a transformer
	// there are lots of scenarios where we can get invalid note message, e.g. multiple note on or
	// note off messages of the same key.
	// so we have 2 strategies to consolidate:
	// 1. a note off message is only valid if it follows (in time) a note on message of the same key and channel and vice versa.
	//    any invalid message is ignored
	// 2. from multiple valid note messages concerning the same key and the same channel, the last wins
	// since there is no concept of a note length, if a transformer generates a note that is already playing
	// and his generated note off message happens before the one for the note that was already playing,
	// his note on message would be ignored, but his note off message not, resulting in a shortening for
	// previous playing note. (which by the way is fine, because it means you can end any running note (e.g. from an
	// arpeggiator) by pressing and releasing it. and most importantly you will never get "hanging" notes.
	// but don't have to consolidate any more, since mid.Writer is doing the job for us!

	for _, msg := range msgs {
		if msg == nil {
			panic("nil messages are not allowed, use StackTick instead")
		}

		if msg != LineTick {
			ctx.wr.writeMessage(ctx, msg)
		}
	}
}

// prepareStackRunning extracts important data for the context before runningn through the stack
func (ctx *smfCtx) prepareStackRunning(msg midi.Message) {
	if msg == meta.EndOfTrack {
		ctx.track++
		// we don't remove the message to just be able to distinguish
		// between just tick calls without a message and calls where something happened
		ctx.runThroughStack(msg)
		return
	}

	switch m := msg.(type) {
	case meta.TimeSig:
		ctx.timeSignature[0] = m.Numerator
		ctx.timeSignature[1] = m.Denominator
		ctx.absTicksoflastTimeSigChange = ctx.tick
		// we don't remove the message to just be able to distinguish
		// between just tick calls without a message and calls where something happened
	case meta.Tempo:
		ctx.tempo = m.FractionalBPM()
		// we don't remove the message to just be able to distinguish
		// between just tick calls without a message and calls where something happened
	}

	ctx.runThroughStack(msg)
}

// prepareAndInjectScheduled injects the scheduled messages
func (ctx *smfCtx) prepareAndInjectScheduled(msg midi.Message) {
	msgs := append(ctx.popScheduledMessages(), msg)

	for _, m := range msgs {
		ctx.prepareStackRunning(m)
	}
}

// gotMessage is called when a message arrives from the reader, which in
// this case should be a SMF reader
func (ctx *smfCtx) gotMessage(pos *mid.Position, msg midi.Message) {
	metric := smf.MetricTicks(ctx.resolution)
	tickdur := metric.FractionalDuration(ctx.tempo, 1)
	lastTick := ctx.tick

	ticks := pos.AbsoluteTicks - lastTick

	if ticks > 1 {
		// call the stack for each intermediate tick
		for i := uint64(0); i < ticks-1; i++ {
			ctx.tick++
			if ctx.sleepAfterTick {
				time.Sleep(tickdur)
			}
			ctx.prepareAndInjectScheduled(LineTick)
		}
	}

	if ticks > 0 {
		ctx.tick++
		if ctx.sleepAfterTick {
			time.Sleep(tickdur)
		}
	}

	ctx.prepareAndInjectScheduled(msg)
}

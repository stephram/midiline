package midiline

import (
	"fmt"
	"io"
	"time"

	"os"
	"runtime"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/smf"
)

//func newPerformanceCtxSlim(in connect.In, out connect.Out, opts ...PerformOption) *performanceCxt {
func newPerformanceCtxSlim(in midi.In, out midi.Out, opts ...mid.ReaderOption) *performanceCxtSlim {
	ctx := &performanceCxtSlim{}
	ctx.stdout = os.Stdout
	ctx.stderr = os.Stderr
	/*
		for _, opt := range opts {
			opt(ctx)
		}
	*/

	ctx.out = out
	ctx.in = in
	//ctx.rd = mid.NewReader(ctx.readerOpts...)
	ctx.rd = mid.NewReader(opts...)
	ctx.rd.Msg.Each = ctx.gotMessage
	//	ctx.rd.Msg.Meta.Tempo = ctx.gotTempoChange
	return ctx
}

type performanceCxtSlim struct {
	// for now in ticks, but maybe musical time like quarternotes
	// defaults to resolution * 40 (10 4/4 bars)
	//resolution                  uint32
	resolution                  smf.MetricTicks
	projectedch                 chan scheduledMessage
	rd                          *mid.Reader
	readerOpts                  []mid.ReaderOption
	st                          *Line
	closer                      chan bool
	messagePipe                 chan midi.Message
	out                         midi.Out
	in                          midi.In
	tempo                       float64
	tick                        uint64
	tickDur                     time.Duration
	lastPos                     *mid.Position
	timeSignature               [2]uint8
	absTicksoflastTimeSigChange uint64
	track                       uint32
	absTickofLastBeatCalc       uint64
	bar                         uint32 // counting from 0
	beat                        uint8  // beat in a bar counting from 0 (in 16th)
	//	stop                        chan bool
	log    bool
	stdout io.Writer
	stderr io.Writer
	writer midi.Writer
}

/*
type performanceMsgSlim struct {
	pos *mid.Position
	msg midi.Message
}
*/

/*
func (ctx *performanceCxtSlim) gotTempoChange(pos mid.Position, bpm uint32) {
	//fmt.Printf("in perform context: got tempo change: %v\n", bpm)
	ctx.readerChannel <- performanceMsg{&pos, meta.Tempo(bpm)}
}
*/

/*
we need a runner in a separate goroutine, that runs each tick throught the stack,
calculates ticks and beats and bars and updates performance context
and also receives incoming messages by a channel.
we need mutex to protect the performance context when updating ticks, etc.
maybe we can skip all these mutex things if we consider the main part of the performance context
to be this runner and just callback connections to the reader to use only go channels
*/

func (ctx *performanceCxtSlim) gotMessage(pos *mid.Position, msg midi.Message) {
	//fmt.Printf("in perform context: got message: %v\n", msg)
	//	ctx.readerChannel <- performanceMsg{pos, msg}
	//	ctx.prepareStackRunning(msg)
	ctx.lastPos = pos
	ctx._prepareStackRunning(msg)
}

func (ctx *performanceCxtSlim) Bar() uint32 {
	return ctx.bar
}

// in MIDI terms, a beat is a 16th note
// maybe we should use that instead of the
// denominator of time signature
func (ctx *performanceCxtSlim) Beat() uint8 {
	return ctx.beat
}

func (ctx *performanceCxtSlim) Tick() uint64 {
	return ctx.tick
}

func (ctx *performanceCxtSlim) Resolution() uint32 {
	return ctx.rd.Resolution()
}

func (ctx *performanceCxtSlim) TempoBPM() float64 {
	// Tempo comes in via MIDI clock and the mid.Reader
	return ctx.tempo
}

// TODO track could mean "take" in the context of a performance
// and could then be usefully changed, when a Stop message arrives
// it would also be necessity to send a EndOfTrack message for recording.
func (ctx *performanceCxtSlim) Track() uint32 {
	return ctx.track
}

func (ctx *performanceCxtSlim) TimeSig() (num, denom uint8) {
	return ctx.timeSignature[0], ctx.timeSignature[1]
}

func (ctx *performanceCxtSlim) runThroughStack(msg midi.Message) {
	//ctx.runThroughStack(msg)

	// don't do the same tick twice!
	if ctx.tick > ctx.absTickofLastBeatCalc {

		// calc the beat and the bar and then run through the stack

		ticksWithinBar := calcTicksWithinBar(
			ctx.tick, ctx.absTicksoflastTimeSigChange,
			uint64(ctx.resolution),
			uint64(ctx.timeSignature[0]), uint64(ctx.timeSignature[1]))

		if ticksWithinBar == 0 {
			// a bar change just happened, but only
			ctx.bar++
			ctx.beat = 0
		} else {
			ctx.beat = calculateBeat(ticksWithinBar, uint32(ctx.resolution.Resolution()))
		}

		ctx.absTickofLastBeatCalc = ctx.tick
	}

	currentMessages := ctx.st._runThroughLine(ctx, msg)

	for _, msg := range currentMessages {
		if msg == nil {
			fmt.Fprintln(ctx.stderr, "nil messages not allowed, use StackTick instead")
			//			panic("nil messages not allowed, use StackTick instead")
		}

		/*
			ctx.writer.Write(msg)
		*/
		if msg != LineTick {
			ctx.messagePipe <- msg
			runtime.Gosched()
		}
	}
}

func (ctx *performanceCxtSlim) _prepareStackRunning(msg midi.Message) {
	if msg == meta.EndOfTrack {
		// not sure, if we should up the track here or based one Stop message
		// ctx.track++
		// we don't remove the message to just be able to distinguish
		// between just tick calls without a message and calls where something happened
		ctx.runThroughStack(msg)
		return
	}

	switch m := msg.(type) {
	case meta.Tempo:
		//fmt.Printf("got tempo: %v\n", m.BPM())
		metric := smf.MetricTicks(ctx.resolution)
		ctx.tickDur = metric.Duration(m.BPM(), 1) // metric.Duration(m.BPM(), 1)
		ctx.tempo = m.FractionalBPM()
	case meta.TimeSig:
		ctx.timeSignature[0] = m.Numerator
		ctx.timeSignature[1] = m.Denominator
		ctx.absTicksoflastTimeSigChange = ctx.tick
	}

	ctx.runThroughStack(msg)

}

func (ctx *performanceCxtSlim) Schedule(deltaticks uint32, msg midi.Message) {
	timestamp := time.Now().Add(ctx.rd.Duration(deltaticks))
	if mm, isMulti := msg.(MultiMessage); isMulti {
		for _, mmm := range mm {
			ctx.projectedch <- scheduledMessage{timestamp, mmm}
		}
	} else {
		ctx.projectedch <- scheduledMessage{timestamp, msg}
	}
}

func (ctx *performanceCxtSlim) Run(ln *Line) (stop chan<- bool, err error) {
	err = ln.Check()
	if err != nil {
		return nil, fmt.Errorf("can't run performance, because an error happened during the line check: %v", err)
	}

	if !ctx.in.IsOpen() {
		err = ctx.in.Open()
		if err != nil {
			return nil, fmt.Errorf("can't open in port: %v", err)
		}

	}

	if !ctx.out.IsOpen() {
		err = ctx.out.Open()
		if err != nil {
			return nil, fmt.Errorf("can't open out port: %v", err)
		}

	}

	ctx.projectedch = make(chan scheduledMessage, 30)
	//	ctx.stop = make(chan bool)
	ctx.closer = make(chan bool)
	//ctx.messagePipe = make(chan midi.Message, 60)
	ctx.messagePipe = make(chan midi.Message)

	//	ctx.readerChannel = make(chan performanceMsg, 10)

	ctx.tempo = 120
	ctx.timeSignature = [2]uint8{4, 4}
	ctx.st = ln
	//ctx.writer = mid.WriteTo(ctx.out)
	sn := &sender{out: mid.ConnectOut(ctx.out), stdout: ctx.stdout}
	//sn.log = ctx.log
	go sn.run(ctx.closer, ctx.messagePipe, ctx.projectedch)
	// TODO we might want to make a reasonable default constant in gomidi/mid, export that and
	// use it here
	ctx.resolution = mid.LiveResolution
	//	metric := smf.MetricTicks(ctx.resolution)
	ctx.tickDur = ctx.resolution.Duration(120, 1) // metric.Duration(120, 1) // default
	//fmt.Printf("starting tickdur: %vns\n", ctx.tickDur.Nanoseconds())
	//	time.Sleep(time.Second)
	//fmt.Println("now sending message")
	err = mid.ConnectIn(ctx.in, ctx.rd)
	if err != nil {
		return nil, err
	}
	go func() {
		<-ctx.closer
		//ctx.in.StopListening()
		ctx.in.Close()
		ctx.out.Close()
		runtime.Gosched()
		//close(ctx.closer)
	}()

	return ctx.closer, nil
}

var _ Context = &performanceCxtSlim{}

package midiline

import (
	"bytes"

	"gitlab.com/gomidi/midi"
)

// lineTick is a fake midi.Message that is only send through a stack
// to activate the Transformers, but never delivered to a midi port or end writer
type lineTick bool

// Raw will panic if called. It must not be called and only exist to fullfill the midi.Message interface.
func (m lineTick) Raw() []byte {
	panic("never call LineTick.Raw, it is supposed to be used within a stack only and should be filtered away from the outside")
}

func (m lineTick) String() string {
	return "midiline.LineTick"
}

const LineTick = lineTick(false)

var _ midi.Message = lineTick(false)

// MultiMessage is a helper pseudo midi.Message that wraps multiple midi messages
// that should be sent at the same time. It can be returned from a callback to be able to
// make multiple midi messages from single ones.
type MultiMessage []midi.Message

var _ midi.Message = MultiMessage{}

// Raw will panic if called. It must not be called and only exist to fullfill the midi.Message interface.
func (m MultiMessage) Raw() []byte {
	panic("don't call MultiMessage.Raw, unpack it instead")
}

// String inspects the MultiMessage for debugging
func (m MultiMessage) String() string {
	var bf bytes.Buffer
	bf.WriteString("<midiline.MultiMessage{")

	for _, mg := range m {
		bf.WriteString(" " + mg.String())
	}

	bf.WriteString(" }")
	return bf.String()
}

package midiline

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/midimessage/meta/meter"
	"gitlab.com/gomidi/midi/midimessage/realtime"
	"gitlab.com/gomidi/midi/midimessage/syscommon"
)

type smfWriter struct {
	wr            *mid.SMFWriter
	tick          uint64
	track         uint32
	tempo         float64
	timeSignature [2]uint8
}

func newSmfWriter(wr *mid.SMFWriter) *smfWriter {
	w := &smfWriter{}
	w.wr = wr
	w.tempo = 120
	w.timeSignature = [2]uint8{4, 4}
	return w
}

func (s *smfWriter) writeMessage(ctx Context, msg midi.Message) {
	if msg == nil {
		panic("nil messages are not allowed, use StackTick instead")
	}

	if msg == LineTick {
		return
	}
	switch msg.(type) {
	case realtime.Message, syscommon.Message:
		// ignore
		return
	}
	if ctx.Tick() > s.tick {
		s.wr.SetDelta(uint32(ctx.Tick() - s.tick))
		s.tick = ctx.Tick()
		if num, denom := ctx.TimeSig(); num != s.timeSignature[0] || denom != s.timeSignature[1] {
			s.timeSignature[0] = num
			s.timeSignature[1] = denom
			s.wr.Write(meter.Meter(num, denom))
		}

		if s.tempo != ctx.TempoBPM() {
			s.tempo = ctx.TempoBPM()
			s.wr.TempoBPM(s.tempo)
		}
	}

	if msg == meta.EndOfTrack {
		s.track++
		s.tick = 0
	}

	s.wr.Write(msg)
}
